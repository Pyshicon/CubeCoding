package me.CubeCoding.wargame.game.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;

public class WarGameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("wargame")) {

            if (args.length < 1) {

                sender.sendMessage("/wargame help");
                return false;
            }

            ArrayList<String> tmp = new ArrayList<>(Arrays.asList(args));
            String id = tmp.get(0);
            tmp.remove(0);
            String[] sub_args = (String[]) tmp.toArray();

            if (id.equalsIgnoreCase("help")) {
                String[] cmds = new String[]{"join, leave"};
                for (String s : cmds) {
                    sender.sendMessage("/wargame " + s);
                }
            } else if (id.equalsIgnoreCase("join")) {
                CommandJoin.execute(sender, id, sub_args);
            } else if (id.equalsIgnoreCase("leave")) {
                CommandLeave.execute(sender, id, sub_args);
            }

        }

        return false;
    }

}
