package me.CubeCoding.wargame.game.commands;

import me.CubeCoding.wargame.WarGame;
import me.CubeCoding.wargame.enums.Messages;
import me.CubeCoding.wargame.game.Arena;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandLeave {
    
    public static boolean execute(CommandSender sender, String cmd, String[] args) {
	
	if(cmd.equalsIgnoreCase("leave")) {
	    
	    if (!(sender instanceof Player)) {
		sender.sendMessage(Messages.CommandSenderError.getMessage());
		return false;
	    }
	    
	    Player player = (Player) sender;
	    Arena arena = WarGame.manager.getArena(player);
	    if (arena == null) {
		player.sendMessage(ChatColor.RED + "You are not part of an arena!");
		return false;
	    }
	    arena.removePlayer(player);
	    player.sendMessage(ChatColor.RED + "You have left the arena!");
	    
	}
	
	return false;
    }

}
