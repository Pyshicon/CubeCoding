package me.CubeCoding.wargame.game.commands;

import me.CubeCoding.wargame.WarGame;
import me.CubeCoding.wargame.enums.Messages;
import me.CubeCoding.wargame.enums.Perms;
import me.CubeCoding.wargame.game.Arena;
import me.CubeCoding.wargame.game.Arena.Team;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandJoin {
    
    public static boolean execute(CommandSender sender, String cmd , String[] args) {
	
	if (cmd.equalsIgnoreCase("join")) {
	    
	    if (!(sender instanceof Player)) {
		sender.sendMessage(Messages.CommandSenderError.getMessage());
		return false;
	    }
	    
	    Player player = (Player) sender;
	    if (!player.hasPermission(Perms.Play.getPerm())) {
		player.sendMessage(Messages.PermissionError.getMessage());
		return false;
	    }
	    
	    Arena arena = WarGame.manager.getArena(args[0]);
	    if (arena == null) {
		player.sendMessage(ChatColor.RED + "Arena not found!");
		return false;
	    }
	    
	    arena.addPlayer(player);
	    Team team = arena.getTeam(player);
	    if (team != null) {
		player.teleport(arena.getSpawn(team));
		player.sendMessage(ChatColor.GOLD + "You have joined the " + ((team == Team.RED ? (ChatColor.RED + "Red") : (ChatColor.BLUE + "Blue")) + " team!"));
	    } else {
		player.sendMessage(Messages.GeneralError.toString());
	    }
	}
	
	return false;
    }

}
