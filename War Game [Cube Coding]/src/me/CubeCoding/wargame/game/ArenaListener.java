package me.CubeCoding.wargame.game;

import me.CubeCoding.wargame.WarGame;
import me.CubeCoding.wargame.game.Arena.ArenaState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class ArenaListener implements Listener {
    
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
	
	Player player = e.getPlayer();
	Arena arena = WarGame.manager.getArena(player);
	if (arena == null) {
	    return;
	}
	if (arena.getState() == ArenaState.COUNTDOWN) {
	    e.setCancelled(true);
	}
	
    }
    
    @EventHandler
    public void onPlayerDamagePlayer(EntityDamageByEntityEvent e) {
	
	if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
	    
	    Player victim = (Player) e.getEntity();
	    Player damager = (Player) e.getDamager();
	    
	    Arena a1 = WarGame.manager.getArena(victim);
	    Arena a2 = WarGame.manager.getArena(damager);
	    
	    if (a1 != a2) {
		return;
	    }
	    
	    if (a1.getTeam(victim) == a2.getTeam(damager)) {
		e.setCancelled(true);
	    }
	    
	}
	
    }

}
