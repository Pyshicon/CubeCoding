package me.CubeCoding.wargame.game;

import me.CubeCoding.wargame.WarGame;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Arena implements Serializable {

    public enum ArenaState {
	COUNTDOWN, FINISHED, STARTED, WAITING
    }

    public enum Team {
	BLUE, RED
    }

    private static final long serialVersionUID = 3625956730195694880L;

    private Location blueSpawn;
    private List<String> blueTeam = new ArrayList<>();
    private String name;
    private Location redSpawn;
    private List<String> redTeam = new ArrayList<>();
    private ArenaState state;
    private Location lobby;

    private int taskid;

    /**
     * The arena object. This contains all data related to a single arena.
     * 
     * @param name
     *            the name of the arena.
     */
    public Arena(String name) {
	this.name = name;
	this.state = ArenaState.WAITING;
    }

    /**
     * Add a player to the arena. Automatically decides best team.
     * 
     * @param p
     *            the player
     */
    public void addPlayer(Player p) {
	if (blueTeam.size() == redTeam.size()) {
	    blueTeam.add(p.getName());
	    return;
	}

	if (blueTeam.size() > redTeam.size()) {
	    redTeam.add(p.getName());
	} else {
	    blueTeam.add(p.getName());
	}
    }

    /**
     * Empty all players from the arena.
     */
    public void clearArena() {
	blueTeam.clear();
	redTeam.clear();
    }

    public boolean hasPlayer(Player player) {

	for (String s : blueTeam) {
	    if (s.equals(player.getName())) {
		return true;
	    }
	}

	for (String s : redTeam) {
	    if (s.equals(player.getName())) {
		return true;
	    }
	}

	return false;
    }

    /**
     * Get an ArrayList<String> of all members of the blue team.
     * 
     * @return blueTeam
     */
    public List<String> getBlueTeam() {
	return blueTeam;
    }

    /**
     * Get the name of the Arena.
     * 
     * @return the name of the arena.
     */
    public String getName() {
	return name;
    }

    /**
     * Get an ArrayList<String> of all members of the red team.
     * 
     * @return redTeam
     */
    public List<String> getRedTeam() {
	return redTeam;
    }

    public Team getTeam(Player player) {
	String name = player.getName();
	for (String s : blueTeam) {
	    if (name.equals(s)) {
		return Team.BLUE;
	    }
	}

	for (String s : redTeam) {
	    if (name.equals(s)) {
		return Team.RED;
	    }
	}
	return null;
    }

    /**
     * Get the spawn for the desired team.
     * 
     * @param t
     *            the team
     * @return the spawn for the team.
     */
    public Location getSpawn(Team t) {
	if (t == Team.RED) {
	    return redSpawn;
	} else {
	    return blueSpawn;
	}
    }

    /**
     * Get the ArenaState for the select Arena.
     * 
     * @return the state of the arena
     */
    public ArenaState getState() {
	return state;
    }

    /**
     * Removes the desired player from the arena.
     * 
     * @param p
     *            the player to remove.
     */
    public void removePlayer(Player p) {
	if (blueTeam.contains(p.getName())) {
	    blueTeam.remove(p.getName());
	}
	if (redTeam.contains(p.getName())) {
	    redTeam.remove(p.getName());
	}
    }

    /**
     * Set the name of the Arena.
     * 
     * @param name
     *            the name of the arena.
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * Sets the spawn for the select team.
     * 
     * @param t
     *            the team to set the spawn for
     * @param loc
     *            the location the spawn will be
     */
    public void setSpawn(Team t, Location loc) {
	if (t == Team.RED) {
	    redSpawn = loc;
	    return;
	}

	if (t == Team.BLUE) {
	    blueSpawn = loc;
	}
    }

    /**
     * Updates the ArenaState of the Arena.
     * 
     * @param state
     *            the new ArenaState
     */
    public void setState(ArenaState state) {
	this.state = state;
    }

    public void start() {
	this.setState(ArenaState.COUNTDOWN);
	for (String s : blueTeam) {
	    Player p = Bukkit.getPlayer(s);
	    p.teleport(blueSpawn);
	}

	for (String s : redTeam) {
	    Player p = Bukkit.getPlayer(s);
	    p.teleport(redSpawn);
	}

	taskid = Bukkit.getScheduler().scheduleSyncRepeatingTask(
		WarGame.getPlugin(), new Runnable() {

		    int timer = 10;

		    @Override
		    public void run() {

			if (timer > 0) {

			    for (String s : blueTeam) {
				Player p = Bukkit.getPlayer(s);
				p.sendMessage(ChatColor.GOLD + "" + timer
					+ " seconds remaining!");
			    }

			    for (String s : redTeam) {
				Player p = Bukkit.getPlayer(s);
				p.sendMessage(ChatColor.GOLD + "" + timer
					+ " seconds remaining!");
			    }
			    timer--;

			} else {

			    for (String s : blueTeam) {
				Player p = Bukkit.getPlayer(s);
				p.sendMessage(ChatColor.GOLD
					+ "Let the games begin!");
			    }

			    for (String s : redTeam) {
				Player p = Bukkit.getPlayer(s);
				p.sendMessage(ChatColor.GOLD
					+ "Let the games begin!");
			    }
			    Bukkit.getScheduler().cancelTask(taskid);

			}

		    }

		}, 0, 20);

    }

    /**
     * Set the lobby location.
     * 
     * @param loc
     *            the location of the lobby.
     */
    public void setLobby(Location loc) {
	this.lobby = loc;
    }

    /**
     * Get the lobby location.
     */
    public Location getLobby() {
	return lobby;
    }

    @Override
    public String toString() {
	return getName();
    }
}
