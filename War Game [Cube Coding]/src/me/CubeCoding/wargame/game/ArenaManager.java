package me.CubeCoding.wargame.game;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ArenaManager {

	private List<Arena> arenas = new ArrayList<>();

	/**
	 * Get a desired arena.
	 * 
	 * @param name
	 *            the name of the arena
	 * @return an arena if not null
	 */
	public Arena getArena(String name) {
		for (Arena a : arenas) {
			if (a.getName().equalsIgnoreCase(name)) {
				return a;
			}
		}
		return null;
	}

	public Arena getArena(Player player) {
	    
	    for (Arena arena : arenas) {
		
		if (arena.hasPlayer(player)) {
		    return arena;
		}
		
	    }
	    return null;
	    
	}
	
	/**
	 * Register an arena.
	 * 
	 * @param a
	 *            the arena.
	 */
	public void registerArena(Arena a) {
		arenas.add(a);
	}

	/**
	 * Unregister an arena.
	 * 
	 * @param name
	 *            the name of the arena.
	 */
	public void unregisterArena(String name) {
		for (Arena a : arenas) {
			if (a.getName().equalsIgnoreCase(name)) {
				arenas.remove(a);
				return;
			}
		}
	}
}
