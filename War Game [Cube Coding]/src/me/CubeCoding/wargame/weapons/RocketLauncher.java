package me.CubeCoding.wargame.weapons;

import me.CubeCoding.wargame.WarGame;
import me.CubeCoding.wargame.enums.Perms;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArrow;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.BlockIterator;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RocketLauncher implements Listener {

    public static ItemStack rocketlauncher = Items.getRocketLauncher();

    private static List<UUID> cooldown = new ArrayList<>();
    private static int power = WarGame.config.getInt("Weapon.Rocket_Launcher.power");
    private static int cooldowntime = WarGame.config.getInt("Weapon.Rocket_Launcher.cooldown");

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {

        Player player = e.getPlayer();
        Action action = e.getAction();
        if (!player.hasPermission(Perms.Rocket.getPerm())) {
            return;
        }
        if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
            ItemStack is = player.getItemInHand();
            if (is.isSimilar(rocketlauncher)) {
                if (cooldown.contains(player.getUniqueId()) && !player.hasPermission(Perms.Cooldown.getPerm())) {
                    player.sendMessage(ChatColor.RED + "Cooldown remaining!");
                } else {
                    launchProjectile(player, RocketLauncher.power);
                }
            }
        }
    }

    @EventHandler
    public void onProjectileHitPlayer(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Arrow) {
            Arrow arrow = (Arrow) e.getDamager();
            if (arrow.hasMetadata("ROCKET")) {
                if (e.getEntity() instanceof LivingEntity) {
                    LivingEntity le = (LivingEntity) e.getEntity();
                    le.getLocation().getWorld().createExplosion(le.getLocation(), power);
                }
            }
        }
    }

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent e) {
        Projectile p = e.getEntity();
        if (p instanceof Arrow) {
            Arrow arrow = (Arrow) p;
            if (p.hasMetadata("ROCKET")) {
                Location loc = arrow.getLocation();
                float power = p.getMetadata("ROCKET").get(0).asFloat();
                loc.getWorld().createExplosion(loc, power);
                arrow.remove();
            }
        }
    }

    private void launchProjectile(Player player, float power) {
        Arrow arrow = player.launchProjectile(Arrow.class);
        arrow.setVelocity(arrow.getVelocity().multiply(3));
        arrow.setMetadata("ROCKET", new FixedMetadataValue(WarGame.getPlugin(), power));

        CraftArrow ca = (CraftArrow) arrow;
        PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(ca.getEntityId());

        BlockIterator iter = new BlockIterator(player.getEyeLocation(), 0, 1000);
        World world = player.getWorld();

        for (Player p : Bukkit.getOnlinePlayers()) {
            CraftPlayer cp = (CraftPlayer) p;
            cp.getHandle().playerConnection.sendPacket(packet);
        }
        while (iter.hasNext()) {
            Location loc = iter.next().getLocation();
            if (loc.getBlock().getType() != Material.AIR) {
                break;
            }
            world.playEffect(loc, Effect.SMOKE, 0, 50);
        }

        if (cooldowntime > 0) {
            cooldown.add(player.getUniqueId());
            Bukkit.getScheduler().scheduleSyncDelayedTask(WarGame.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    cooldown.remove(player.getUniqueId());
                }
            }, cooldowntime * 20);
        }
    }

}