package me.CubeCoding.wargame.weapons;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Bolt on 7/1/2015.
 */
public class Items {

	private static String prefix = ChatColor.GRAY + "" + ChatColor.BOLD;

	public static ItemStack getAWP() {
		ItemStack awp = new ItemStack(Material.IRON_BARDING);
		ItemMeta meta = Bukkit.getItemFactory().getItemMeta(Material.IRON_BARDING);
		meta.setDisplayName(prefix + "AWP");
		awp.setItemMeta(meta);
		return awp;
	}

	public static ItemStack getDesertEagle() {
		ItemStack dg = new ItemStack(Material.IRON_BARDING);
		ItemMeta meta = Bukkit.getItemFactory().getItemMeta(Material.IRON_BARDING);
		meta.setDisplayName(prefix + "Desert Eagle");
		dg.setItemMeta(meta);
		return dg;
	}

	public static ItemStack getGattlingGun() {
		ItemStack gun = new ItemStack(Material.GOLD_BARDING);
		ItemMeta meta = Bukkit.getItemFactory().getItemMeta(Material.GOLD_BARDING);
		meta.setDisplayName(prefix + "Gattling Gun");
		gun.setItemMeta(meta);
		return gun;
	}

	public static ItemStack getRocketLauncher() {
		ItemStack rocketlauncher = new ItemStack(Material.IRON_BARDING);
		ItemMeta meta = Bukkit.getItemFactory().getItemMeta(Material.IRON_BARDING);
		meta.setDisplayName(prefix + "Rocket Launcher");
		rocketlauncher.setItemMeta(meta);
		return rocketlauncher;
	}

}
