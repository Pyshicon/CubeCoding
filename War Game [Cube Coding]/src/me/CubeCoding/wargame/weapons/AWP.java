package me.CubeCoding.wargame.weapons;

import me.CubeCoding.wargame.WarGame;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AWP implements Listener {

	public static ItemStack awp = Items.getAWP();

	// Cooldowns
	private static List<UUID> cooldown = new ArrayList<UUID>();
	private static int cooldowntime = WarGame.config.getInt("Weapon.AWP.cooldown");
	private static double damage = WarGame.config.getDouble("Weapon.AWP.damage");

	// Desert Eagle

	@EventHandler
	public void onZoom(PlayerInteractEvent z) {
		final Player p = z.getPlayer();
		final Location loc = p.getLocation();
		if (z.getItem().equals(awp)) {
			if (z.getAction() == Action.LEFT_CLICK_AIR || z.getAction() == Action.LEFT_CLICK_BLOCK) {
				if (!p.hasPotionEffect(PotionEffectType.SLOW)) {
					p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000000019, 1));
					p.playSound(loc, Sound.PISTON_EXTEND, 1, 1);
				} else {
					p.removePotionEffect(PotionEffectType.SLOW);
				}
			}
		}
	}

	@EventHandler
	public void onShoot(EntityDamageByEntityEvent d) {
		Snowball s = (Snowball) d.getDamager();
		Player shooter = (Player) s.getShooter();
		if (d.getDamager().getType() == EntityType.SNOWBALL && shooter.getItemInHand().equals(awp)) {
			d.setDamage(damage); // Default 15.0
		}
	}

	// Items shooting event
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		Location loc = p.getLocation();
		if (!cooldown.contains(e.getPlayer().getName())) {
			if (p.getItemInHand().isSimilar(awp)) {
				if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					createAWP(damage, p);
					p.playSound(loc, Sound.WITHER_SHOOT, 1, 1);
					cooldown.add(p.getUniqueId());
					WarGame.getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(WarGame.getPlugin(), new Runnable() {
						public void run() {
							cooldown.remove(p.getUniqueId()); // Removing cooldown
						}
					}, 20 * cooldowntime); // 20 = 1 second
				}
			}
		}
	}


	private void createAWP(double power, Player p) {
		Snowball sb = p.launchProjectile(Snowball.class);
		sb.setShooter(p);
		sb.setVelocity(p.getLocation().getDirection().multiply(power));
	}
}
