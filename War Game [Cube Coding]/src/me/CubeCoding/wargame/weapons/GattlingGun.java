package me.CubeCoding.wargame.weapons;

import me.CubeCoding.wargame.WarGame;
import me.CubeCoding.wargame.enums.Perms;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GattlingGun implements Listener {

    public static ItemStack gun = new ItemStack(Material.GOLD_BARDING);

    private static double damage = WarGame.config.getDouble("Weapons.Gattling_Gun.damage");
    private static int speed = WarGame.config.getInt("Weapons.Gattling_Gun.speed");

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        Action action = e.getAction();
        if (!player.hasPermission(Perms.Gattling.getPerm())) {
            return;
        }
        if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
            ItemStack is = player.getItemInHand();
            if (is.isSimilar(gun)) {
                launchProjectile(player);
            }
        }
    }

    @EventHandler
    public void onProjectileHitPlayer(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player && e.getDamager() instanceof Snowball) {
            Player player = (Player) e.getEntity();
            Snowball snowball = (Snowball) e.getDamager();
            player.damage(damage);
            e.setCancelled(true);
        }
    }

    private void launchProjectile(Player player) {
        for (int i = 0; i < speed; i++) {
            Snowball sb = player.launchProjectile(Snowball.class);
            sb.setMetadata("GATTLING", new FixedMetadataValue(WarGame.getPlugin(), null));
        }
    }

}
