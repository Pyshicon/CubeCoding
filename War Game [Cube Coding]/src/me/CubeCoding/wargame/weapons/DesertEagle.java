package me.CubeCoding.wargame.weapons;

import me.CubeCoding.wargame.WarGame;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DesertEagle implements Listener {

	public static ItemStack dg = new ItemStack(Material.IRON_BARDING);

	// Cooldowns
	public static List<UUID> cooldown = new ArrayList<>();
	private static int cooldowntime = WarGame.config.getInt("Weapon.Desert_Eagle.cooldown");
	private static double damage = WarGame.config.getDouble("Weapon.Desert_Eagle.damage");
	// Desert Eagle	

	public void onProjectileHit(EntityDamageByEntityEvent d) {
		Snowball s = (Snowball) d.getDamager();
		Player shooter = (Player) s.getShooter();
		if (d.getDamager().getType() == EntityType.SNOWBALL && shooter.getItemInHand().equals(dg)) {
			d.setDamage(damage); // Default 5.0
		}
	}

	// Items shooting event
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		Location loc = p.getLocation();
		if (!cooldown.contains(e.getPlayer().getName())) {
			if (p.getItemInHand().isSimilar(dg)) {
				if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					createDesertEagle(damage, p);
					p.playSound(loc, Sound.WITHER_SHOOT, 1, 1);
					cooldown.add(p.getUniqueId());
					WarGame.getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(WarGame.getPlugin(), new Runnable() {
						public void run() {
							cooldown.remove(p.getUniqueId()); // Removing cooldown
						}
					}, 20 * cooldowntime); // 20 = 1 second
				}
			}
		}
	}


	private void createDesertEagle(double power, Player p) {
		Snowball sb = p.launchProjectile(Snowball.class);
		sb.setShooter(p);
		sb.setVelocity(p.getLocation().getDirection().multiply(power));

	}
}
