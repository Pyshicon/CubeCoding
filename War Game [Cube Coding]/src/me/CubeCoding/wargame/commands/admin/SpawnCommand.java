package me.CubeCoding.wargame.commands.admin;

import me.CubeCoding.wargame.CommandInfo;
import me.CubeCoding.wargame.WarGame;
import me.CubeCoding.wargame.enums.Messages;
import me.CubeCoding.wargame.game.Arena;
import me.CubeCoding.wargame.game.Arena.Team;
import me.CubeCoding.wargame.game.GameCommand;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

@CommandInfo(aliases = { "setspawn" })
public class SpawnCommand extends GameCommand {
	Permission admin = new Permission("wargame.admin");

	@Override
	public void onCommand(Player p, String[] args) {
		if (!p.hasPermission(admin)) {
			p.sendMessage(Messages.PermissionError.getMessage());
			return;
		}

		if (args.length == 2) {
			Arena a = WarGame.manager.getArena(args[0]);
			if (a == null) {
				p.sendMessage(ChatColor.RED + "Arena not found!");
				return;
			}

			switch (args[1].toLowerCase()) {
			case "red":
				a.setSpawn(Team.RED, p.getLocation());
				p.sendMessage(ChatColor.RED + "Red " + ChatColor.GOLD
						+ "team spawn set!");
				return;
			case "blue":
				a.setSpawn(Team.BLUE, p.getLocation());
				p.sendMessage(ChatColor.AQUA + "Blue " + ChatColor.GOLD
						+ "team spawn set!");
				return;
			default:
				p.sendMessage(ChatColor.RED + "Unknown team: "
						+ ChatColor.RESET + args[1]);
				return;
			}
		}
	}

}
