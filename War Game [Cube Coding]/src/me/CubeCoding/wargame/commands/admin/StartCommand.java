package me.CubeCoding.wargame.commands.admin;

import me.CubeCoding.wargame.WarGame;
import me.CubeCoding.wargame.enums.Messages;
import me.CubeCoding.wargame.game.Arena;
import me.CubeCoding.wargame.game.Arena.ArenaState;
import me.CubeCoding.wargame.game.GameCommand;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;

public class StartCommand extends GameCommand {

	@Override
	public void onCommand(Player p, String[] args) {
		if (args.length == 1) {
			Arena a = WarGame.manager.getArena(args[0]);
			if (a == null) {
				p.sendMessage(ChatColor.RED + "Arena not found!");
				return;
			}

			if (a.getState() == ArenaState.STARTED) {
				p.sendMessage(ChatColor.RED + "Arena is already running!");
				return;
			}

			a.start();
			p.sendMessage(ChatColor.GOLD + "Arena has been started!");
			return;
		} else {
			p.sendMessage(Messages.WrongUsageError.getMessage());
		}
	}

}
