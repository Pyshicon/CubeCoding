package me.CubeCoding.wargame.enums;

public enum Perms {

    Play("play"),
    Setspawn("admin.setspawn"),
    Cooldown("weapons.cooldown"), // Bypass cooldown
    Rocket("weapons.rocket"),
    Gattling("weapons.gattling");

    String perm;
    String prefix = "wargame.";

    Perms(String perm) {
        this.perm = perm;
    }

    public String getPerm() {
        return perm;
    }

}
