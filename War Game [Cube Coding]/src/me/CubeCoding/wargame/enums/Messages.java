package me.CubeCoding.wargame.enums;

import org.bukkit.ChatColor;

public enum Messages {
    
    Sample("Sample message"),
    CommandSenderError(ChatColor.RED + "You can only use this command in-game!"),
    PermissionError(ChatColor.RED + "You don't have permission to do this!"),
    GeneralError(ChatColor.RED + "An error occurred."),
    WrongUsageError(ChatColor.RED+ "Incorrect usage!");

	String msg;

	Messages(String msg) {
		this.msg = msg;
	}

	public String getMessage() {
		return msg;
	}

}
