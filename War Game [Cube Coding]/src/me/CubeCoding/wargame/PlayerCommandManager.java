package me.CubeCoding.wargame;

import me.CubeCoding.wargame.commands.admin.SpawnCommand;
import me.CubeCoding.wargame.enums.Messages;
import me.CubeCoding.wargame.game.GameCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class PlayerCommandManager {
	private ArrayList<GameCommand> gcs = new ArrayList<GameCommand>();

	protected PlayerCommandManager() {
		gcs.add(new SpawnCommand());
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Messages.CommandSenderError.getMessage());
			return false;
		}

		Player p = (Player) sender;

		for (GameCommand gc : gcs) {
			CommandInfo info = gc.getClass().getAnnotation(CommandInfo.class);
			for (String n : info.aliases()) {
				if (n.equalsIgnoreCase(cmd.getName())) {
					gc.onCommand(p, args);
				}
			}
		}

		return true;
	}
}