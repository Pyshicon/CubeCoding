package me.CubeCoding.wargame;

import me.CubeCoding.wargame.game.ArenaManager;
import me.CubeCoding.wargame.game.commands.WarGameCommand;
import me.CubeCoding.wargame.utils.Colors;
import me.CubeCoding.wargame.weapons.DesertEagle;
import me.CubeCoding.wargame.weapons.GattlingGun;
import me.CubeCoding.wargame.weapons.RocketLauncher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;

public class WarGame extends JavaPlugin implements Listener {

	public static FileConfiguration config;
	public static File configf;

	public static ArenaManager manager = new ArenaManager();

	private Colors colormanager;
	public static String pre = ChatColor.BLACK + "[" + ChatColor.YELLOW
			+ "WarGame" + ChatColor.BLACK + "]";

	private static Plugin plugin;

	public void onEnable() {
		createFiles();
		// File configfile = new File(this.getDataFolder() + File.separator +
		// "config.yml");
		// FileConfiguration config =
		// YamlConfiguration.loadConfiguration(configfile);
		// config.addDefault("Weapon.Desert_Eagle.Damage", "5.0");
		// config.addDefault("Weapon.AWP.Damage", "15.0");
		// saveConfig();

		// Create a template file and save it in config instead of above ^^

		setColorTheme();

		// Use this.getCommand("command").setExecutor(new Class()); to register
		// command
		// Use Bukkit.getServer().getPluginManager().registerEvents(new Class(),
		// this); to register the events
		this.getCommand("wargame").setExecutor(new WarGameCommand());

		Bukkit.getServer().getPluginManager().registerEvents(new DesertEagle(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new RocketLauncher(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new GattlingGun(), this);
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		plugin = this;

	}

	private void setColorTheme() {
		if (config.get("global.colorscheme") == String.valueOf(1)) {

		} else if (config.get("global.colorscheme") == String.valueOf(2)) {

		} else if (config.get("global.colorscheme") == String.valueOf(3)) {

		}

	}

	public void onDisable() {
		plugin = null;

	}

	public static void registerEvents(org.bukkit.plugin.Plugin plugin,
	                                  Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager()
					.registerEvents(listener, plugin);
		}
	}

	public static Plugin getPlugin() {
		return plugin;

	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
	                         String[] args) {
		new PlayerCommandManager().onCommand(sender, cmd, label, args);

		return true;
	}

	public void createFiles() {

		if (!getDataFolder().exists()) {
			getDataFolder().mkdir();
		}

		configf = new File(getDataFolder(), "config.yml");
		if (!configf.exists()) {
			try {
				configf.createNewFile();
				copy(getResource("config.yml"), configf);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		config = YamlConfiguration.loadConfiguration(configf);

	}

	private static void copy(InputStream in, File file) {

		try {

			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {

				out.write(buf, 0, len);

			}
			out.close();
			in.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

	}
}
